import {storage} from '@core/utils';

/**
 *
 * @param {string} key
 * @return {string}
 */
function toHTML(key) {
	const model = storage(key);
	const id = key.split(':')[1];
	const href = `excel/${id}`;
	return `
		<li class="db__record">
			<a href="#${href}">${model.title}</a>
			<strong>
				${new Date(model.openDate).toLocaleDateString()}
				${new Date(model.openDate).toLocaleTimeString()}
			</strong>
		</li>`;
}


/**
 *
 * @return {[]}
 */
function getAllKeys() {
	const keys = [];
	for (let i = 0; i < localStorage.length; i++) {
		const key = localStorage.key(i);
		if (!key.includes('excel')) {
			continue;
		}
		keys.push(key);
	}
	return keys;
}

/**
 *
 * @return {string}
 */
export function getAllRecords() {
	const keys = getAllKeys();

	if (!keys.length) {
		return `<p>Таблиц нет</p>`;
	}

	return `<div class="db__list-header">
    <span>Название</span>
    <span>Дата открытия</span>
  </div>

  <ul class="db__list">
  	${keys.map(toHTML).join('')}
  </ul>`;
}
