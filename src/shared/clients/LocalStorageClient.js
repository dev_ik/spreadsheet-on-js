import {storage} from '@core/utils';

/**
 * @class LocalStorageClient
 */
export class LocalStorageClient {
	/**
	 * @method constructor
	 *
	 * @param {string} name
	 */
	constructor(name) {
		this.name = storageName(name);
	}

	/**
	 * @method save
	 *
	 * @param {Object} state
	 * @return {Promise<void>}
	 */
	save(state) {
		storage(this.name, state);
		return Promise.resolve();
	}

	/**
	 * @method get
	 *
	 * @return {Promise}
	 */
	get() {
		return Promise.resolve(storage(this.name));
	}
}

/**
 *
 * @param {string} param
 * @return {string}
 */
function storageName(param) {
	return 'excel:' + param;
}
