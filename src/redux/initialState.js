import {defaultStyles, defaultTitle} from '@/constans';
import {clone} from '@core/utils';

/**
 *
 * @type {
 * 	 {
 *	 	 currentText: string,
 * 	 	 rowState: {},
 * 	 	 dataState: {},
 * 	 	 colState: {},
 * 	 	 stylesState: {},
 * 		 title: string,
 * 	 	 currentStyle: {
 * 	 			textAlign: string,
 * 	 			textDecoration: string,
 * 	 			fontStyle: string,
 * 	 			fontWeight: string
 * 	 		},
 * 		 openDate: string
 * 		}
 * 	}
 */
const defaultState = {
	title: defaultTitle,
	rowState: {},
	colState: {},
	dataState: {},
	stylesState: {},
	currentText: '',
	currentStyle: defaultStyles,
	openDate: new Date().toString(),
};

/**
 *
 * @param {Object} state
 *
 * @return {Object}
 */
const normalize = state => ({
	...state,
	currentStyle: defaultStyles,
	currentText: '',
});

/**
 *
 * @param {Object} state
 * @return {*}
 */
export function normalizeInitialState(state) {
	return state ? normalize(state) : clone(defaultState);
}
