import {
	TABLE_RESIZE,
	CHANGE_TEXT,
	CHANGE_STYLES,
	APPLY_STYLE,
	CHANGE_TITLE,
	UPDATE_DATE,
} from './types';

/**
 *
 * @param {*} data
 *
 * @return {Object}
 */
export function tableResize(data) {
	return {
		type: TABLE_RESIZE,
		data,
	};
}

/**
 *
 * @param {*} data
 *
 * @return {Object}
 */
export function changeText(data) {
	return {
		type: CHANGE_TEXT,
		data,
	};
}

/**
 *
 * @param {*} data
 * @return {{data: *, type: string}}
 */
export function changeStyles(data) {
	return {
		type: CHANGE_STYLES,
		data,
	};
}

/**
 *
 * @param {Object} data
 * @return {{data: *, type: string}}
 */
export function applyStyle(data) {
	return {
		type: APPLY_STYLE,
		data,
	};
}

/**
 *
 * @param {string} data
 * @return {{data: *, type: string}}
 */
export function changeTitle(data) {
	return {
		type: CHANGE_TITLE,
		data,
	};
}

/**
 *
 * @return {{type: string}}
 */
export function updateDate() {
	return {
		type: UPDATE_DATE,
	};
}
