import {toInlineStyles} from '@core/utils';
import {defaultStyles} from '@/constans';
import {parse} from '@core/parse';

/**
 *
 * @type {{A: number, Z: number}}
 */
const CODES = {
	A: 65,
	Z: 90,
};

/**
 *
 * @type {number}
 */
const DEFAULT_WIDTH = 120;

/**
 *
 * @type {number}
 */
const DEFAULT_HEIGHT = 24;

/**
 *
 * @param {state} state
 * @param {*} row
 *
 * @return {Function}
 */
function createCell(state, row) {
	return function(_, col) {
		const idCell = `${row}:${col}`;
		const width = getWidth(state.colState, col);
		const data = state.dataState[idCell];
		const styles = toInlineStyles({
			...defaultStyles,
			...state.stylesState[idCell],
		});
		return `
		<div class="excel__table-row_data-cell" contenteditable="true"
		 	data-col="${col}"
			data-id="${idCell}" 
			data-type="cell"
			data-value="${data || ''}"
			style = "${styles}; width:${width}"
		>
		${parse(data) || ''}
 	</div>
		`;
	};
}

/**
 * @param {string} contentColumn
 * @param {number} index
 * @param {string} width
 *
 * @return {string}
 */
function createCol({contentColumn, index, width}) {
	return `
	<div class="excel__table-row_data-column" 
		data-type="resizeble" data-col="${index}" style="width:${width}">
		${contentColumn}
		<div class="col-resize" data-resize="col"></div>
	</div>	
	`;
}

/**
 * @param {number} index
 * @param {string} content
 * @param {Object} state
 *
 * @return {string}
 */
function createRow(index, content, state) {
	const height = getHeight(state, index);
	const resizeTemplate = index
		? `<div class="row-resize" data-resize="row"></div>`
		: '';
	return `
	<div class="excel__table-row" 
		data-type="resizeble" 
		data-row="${index}" 
		style="height:${height}"
	>
	<div class="excel__table-row_info">
		${index ? index : ''}
		${resizeTemplate}
	</div>
	<div class="excel__table-row_data">${content}</div>
	</div>
	`;
}
/**
 *
 * @param {*} el
 * @param {*} index
 *
 * @return {string}
 */
function toChar(el, index) {
	return String.fromCharCode(CODES.A + index);
}

/**
 *
 * @param {*} colsState
 * @param {*} index
 *
 * @return {string};
 */
function getWidth(colsState = {}, index) {
	return (colsState[index] || DEFAULT_WIDTH) + 'px';
}

/**
 *
 * @param {*} rowState
 * @param {*} index
 *
 * @return {string};
 */
function getHeight(rowState = {}, index) {
	return (rowState[index] || DEFAULT_HEIGHT) + 'px';
}

/**
 *
 * @param {*} state
 *
 * @return {Function}
 */
function widthFromSate(state) {
	return function(contentColumn, index) {
		return {
			contentColumn, index, width: getWidth(state.colState, index),
		};
	};
}

/**
 *
 * @param {*} rowsCount
 * @param {state} state
 * @return {string}
 */
export function createTable(rowsCount = 15, state) {
	const colsCount = CODES.Z - CODES.A + 1;
	const rows = [];
	const cols = new Array(colsCount)
		.fill('')
		.map(toChar)
		.map(widthFromSate(state))
		.map(createCol)
		.join('');

	rows.push(createRow(null, cols));

	for (let row = 0; row < rowsCount; row++) {
		const colsForRow = new Array(colsCount)
			.fill('')
			.map(createCell(state, row))
			.join('');
		rows.push(createRow(row + 1, colsForRow, state.rowState));
	}

	return rows.join('');
}
