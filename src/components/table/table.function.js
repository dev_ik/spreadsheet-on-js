/**
 *
 * @param {Event} event
 *
 * @return {boolean}
 */
export function canResize(event) {
	return event.target.dataset.resize;
}

/**
 *
 * @param {Event} event
 *
 * @return {boolean}
 */
export function isCell(event) {
	return event.target.dataset.type === 'cell';
}
