/**
 * @class TableSelection
 */
export class TableSelection {
	static selectedClassName = 'excel__table-row_data-cell--selected';

	/**
   * @method constructor
   */
	constructor() {
		this.group = [];
		this.current = null;
	}

	/**
	 * @method select
	 *
   * @param {Element} $el
   */
	select($el) {
		this.clear();
		this.group.push($el);
		this.current = $el;
		$el.focus().addClass(TableSelection.selectedClassName);
	}

	/**
	 * @method clear
	 */
	clear() {
		this.group.forEach($el => $el
			.removeClass(TableSelection.selectedClassName));
		this.group = [];
	}

	/**
	 * @method SelectedIds
	 *
	 * @return {[]}
	 */
	get selectedIds() {
		return this.group.map( $el => $el.id());
	}

	/**
	 * @method selectGroup
	 *
   * @param {Array} $group
   */
	selectGroup($group = []) {
		this.clear();
		this.group = $group;

		this.group.forEach($el => $el.addClass(TableSelection.selectedClassName));
	}

	/**
	 * @method applyStyle
	 *
	 * @param {Object} style
	 */
	applyStyle(style) {
		this.group.forEach( $el => $el.css(style));
	}
}
