import {ExcelComponent} from '@core/ExcelComponent';
import {createTable} from './table.template';
import {resizeHandler} from './table.resize';
import {canResize, isCell} from './table.function';
import {TableSelection} from './TableSelection';
import {$} from '@/core/dom';
import {matrix, nextSelector} from '@/core/utils';
import * as actions from '@/redux/actions';
import {defaultStyles} from '@/constans';
import {parse} from '@core/parse';

/**
 * @class Table
 */
export class Table extends ExcelComponent {
	static className = 'excel__table';

	/**
	 * @method constructor
	 *
	 * @param {*} $root
	 * @param {*} options
	 */
	constructor($root, options) {
		super($root, {
			name: 'Table',
			listeners: ['mousedown', 'keydown', 'input'],
			...options,
		});
	}

	/**
	 * @method toHTML
	 *
   * @return {string}
   */
	toHTML() {
		return createTable(20, this.store.getState());
	}

	/**
	 * @method prepare
	 */
	prepare() {
		this.selection = new TableSelection();
	}

	/**
	 * @method init
	 */
	init() {
		super.init();
		this.selectCell(this.$root.find('[data-id="0:0"]'));

		this.$on('formula:input', value => {
			this.parseCurrentCell(value);
			this.updateTextInStore(value);
		});

		this.$on('formula:done', () => {
			this.selection.current.focus();
		});

		this.$on('toolbar:applyStyle', value => {
			this.selection.applyStyle(value);
			this.$dispatch(actions.applyStyle( {
				value,
				ids: this.selection.selectedIds,
			}));
		});
	}

	/**
	 * @method resizeTable
	 *
	 * @param {*} event
	 */
	async resizeTable(event) {
		try {
			const data = await resizeHandler(this.$root, event);
			this.$dispatch(actions.tableResize(data));
		} catch (e) {
			console.warn(e.message);
		}
	}

	/**
	 * @method onMousedown
	 *
	 * @param {MouseEvent} event
	 */
	onMousedown(event) {
		if (canResize(event)) {
			this.resizeTable(event);
		} else if (isCell(event)) {
			const $target = $(event.target);
			if (event.shiftKey) {
				const $cells = matrix($target, this.selection.current)
					.map(id => this.$root.find(`[data-id="${id}"]`));

				this.selection.selectGroup($cells);
			} else {
				this.selectCell($target);
			}
		}
	}

	/**
	 * @method selectCell
	 *
	 * @param {Dom} $cell
	 */
	selectCell($cell) {
		this.selection.select($cell);
		this.$emit('table:select', $cell);
		const styles = $cell.getStyles(Object.keys(defaultStyles));
		this.$dispatch(actions.changeStyles(styles));
	}

	/**
	 * @method onKeydown
	 *
	 * @param {*} event
	 */
	onKeydown(event) {
		const keys = [
			'Enter',
			'Tab',
			'ArrowLeft',
			'ArrowRight',
			'ArrowDown',
			'ArrowUp',
		];

		const {key} = event;

		if (keys.includes(key) && !event.shiftKey) {
			event.preventDefault();
			this.parseCurrentCell(this.selection.current.text() || '');
			const id = this.selection.current.id(true);
			const $next = this.$root.find(nextSelector(key, id));

			this.selectCell($next);
		}
	}

	/**
	 * @method parseCurrentCell
	 *
	 * @param {string} value
	 */
	parseCurrentCell(value = '') {
		if (value) {
			this.selection.current
				.attr('data-value', value)
				.text(parse(value));
		}
	}

	/**
	 * @method updateTextInStore
	 *
	 * @param {*} value
	 */
	updateTextInStore(value) {
		this.$dispatch(actions.changeText({
			id: this.selection.current.id(),
			value,
		}));
	}

	/**
	 * @method onInput
	 *
	 * @param {Event} event
	 */
	onInput(event) {
		this.updateTextInStore($(event.target).text());
	}
}
