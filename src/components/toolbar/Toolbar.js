import {createToolbar} from './toolbar.template';
import * as toolbarTypes from './toolbar.types';
import {$} from '@/core/dom';
import {ExcelStateComponent} from '@core/ExcelStateComponent';
import {defaultStyles} from '@/constans';

/**
 * @class Toolbar
 */
export class Toolbar extends ExcelStateComponent {
	static className = 'excel__toolbar';

	/**
	 *
	 * @param {*} $root
	 * @param {*} options
	 */
	constructor($root, options) {
		super($root, {
			name: 'Toolbar',
			listeners: ['click'],
			subscribe: ['currentStyle'],
			...options,
		});
	}

	/**
	 *
	 */
	prepare() {
		this.initState(defaultStyles);
	}

	/**
	 *
	 * @return {string}
	 */
	get template() {
		return createToolbar(this.state);
	}

	/**
  * @return {string}
  */
	toHTML() {
		return this.template;
	}

	/**
	 *
	 * @param {Object} changes
	 */
	storeChanged(changes) {
		super.setState(changes.currentStyle);
	}

	/**
	 *
	 * @param {Event} event
	 */
	onClick(event) {
		const $target = $(event.target);
		if ($target.data.type === toolbarTypes.DATA_BUTTON) {
			const value = JSON.parse($target.data.value);
			this.$emit('toolbar:applyStyle', value);
		}
	}
}
