import * as toolbarTypes from './toolbar.types';
/**
 *
 * @param {Object} button
 *
 * @return {string}
 */
function templateButton(button) {
	const dataAttr = `
    data-type="${toolbarTypes.DATA_BUTTON}"
    data-value='${JSON.stringify(button.value)}'
  `;
	return `
  <div
    ${dataAttr}
    class="excel__toolbar-button ${button.active ? 'active' : ''}">
    <i ${dataAttr} class="material-icons">
      ${button.icon}
    </i>
  </div>`;
}

/**
 *
 * @param {Object} state
 * @return {string}
 */
export function createToolbar(state) {
	const buttons = [
		{
			icon: 'format_align_left',
			active: state['textAlign'] === 'left',
			value: {textAlign: 'left'},
		},
		{
			icon: 'format_align_center',
			active: state['textAlign'] === 'center',
			value: {textAlign: 'center'},
		},
		{
			icon: 'format_align_right',
			active: state['textAlign'] === 'right',
			value: {textAlign: 'right'},
		},
		{
			icon: 'format_bold',
			active: state['fontWeight'] === 'bold',
			value: {fontWeight: state['fontWeight'] === 'bold' ? 'normal' : 'bold'},
		},
		{
			icon: 'format_italic',
			active: state['fontStyle'] === 'italic',
			value: {fontStyle: state['fontStyle'] === 'italic' ? 'normal' : 'italic'},
		},
		{
			icon: 'format_underline',
			active: state['textDecoration'] === 'underline',
			value: {textDecoration: state['textDecoration'] === 'underline'
				? 'none' : 'underline'},
		},
	];
	return buttons.map(templateButton).join('');
}
