import {$} from '../core/dom';

/**
 *
 * @return {string}
 */
export function Loader() {
	return $.create('div', 'loader')
		.html(`<div class="lds-spinner">
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
									<div></div>
								<div>`);
}
