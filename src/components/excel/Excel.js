import {$} from '@core/dom';
import {Emmiter} from '@/core/Emmiter';
import {StoreSubscribe} from '@/core/StoreSubscriber';
import {updateDate} from '@/redux/actions';
import {preventDefault} from '@core/utils';

/**
 * @class Excel
 */
export class Excel {
	/**
   * @method constructor
	 *
   * @param {*} options,
   */
	constructor(options) {
		this.components = options.components || [];
		this.store = options.store;
		this.emmiter = new Emmiter();
		this.subscriber = new StoreSubscribe(this.store);
	}

	/**
	 * @method getRoot
	 *
	 * @return {string}
	 */
	getRoot() {
		const $root = $.create('div', 'excel');

		const componentOptions = {
			emmiter: this.emmiter,
			store: this.store,
		};

		this.components = this.components.map(Component => {
			const $el = $.create('div', Component.className);
			const component = new Component($el, componentOptions);
			$el.html(component.toHTML());
			$root.append($el);
			return component;
		});

		return $root;
	}

	/**
	 * @method init
	 */
	init() {
		if (process.env.NODE_ENV === 'production') {
			document.addEventListener('contextmenu', preventDefault);
		}
		this.store.dispatch(updateDate());
		this.subscriber.subscribeComponents(this.components);
		this.components.forEach(component => component.init());
		document.removeEventListener('contextmenu', preventDefault);
	}

	/**
	 * @method destroy
	 */
	destroy() {
		this.subscriber.unsubscribeFromStore();
		this.components.forEach(component => component.destroy());
	}
}
