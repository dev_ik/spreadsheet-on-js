import {ExcelComponent} from '@core/ExcelComponent';
import {changeTitle} from '@/redux/actions';
import {defaultTitle} from '@/constans';
import {debounce} from '@core/utils';
import {ActiveRoute} from '@core/routs/ActiveRoute';
import {$} from '@core/dom';

/**
 * @class Header
 */
export class Header extends ExcelComponent {
	static className = 'excel__header';

	/**
	 * @method constructor
	 *
	 * @param {*} $root
	 * @param {*} options
	 */
	constructor($root, options) {
		super($root, {
			name: 'Header',
			listeners: ['input', 'click'],
			...options,
		});
	}

	/**
	 * @method prepare
	 */
	prepare() {
		this.onInput = debounce(this.onInput, 300);
	}

	/**
	 * @method toHTML
	 *
   * @return {string}
   */
	toHTML() {
		const title = this.store.getState().title || defaultTitle;
		return `
		<input type="text" class="excel__header-input" value="${title}">
		<div>
			<div data-action="remove" class="excel__header-button">
				<i data-action="remove" class="material-icons">delete</i>
			</div>
			<div data-action="exit" class="excel__header-button">
				<i data-action="exit" class="material-icons">exit_to_app</i>
			</div>
		</div>`;
	}

	/**
	 * @method onInput
	 *
	 * @param {Event} event
	 */
	onInput(event) {
		const $target = event.target;
		this.$dispatch(changeTitle($target.value));
	}

	/**
	 * @method onClick
	 *
	 * @param {Event} event
	 */
	onClick(event) {
		const $target = $(event.target);
		if ($target.data.action === 'remove') {
			const decision = confirm('Вы действительно хотите ' +
					'удалить данную таблицу?');
			if (decision) {
				localStorage.removeItem(`excel:${ActiveRoute.param}`);
				ActiveRoute.navigate('');
			}
		} else if ($target.data.action === 'exit') {
			ActiveRoute.navigate('');
		}
	}
}
