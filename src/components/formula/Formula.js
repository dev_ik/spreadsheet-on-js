import {ExcelComponent} from '@core/ExcelComponent';
import {$} from '@/core/dom';

/**
 * @class Formula
 */
export class Formula extends ExcelComponent {
	static className = 'excel__formula';

	/**
	 * @method constructor
	 *
	 * @param {*} $root
	 * @param {*} options
	 */
	constructor($root, options) {
		super($root, {
			name: 'Formula',
			listeners: [
				'input',
				'keydown',
			],
			subscribe: ['currentText'],
			...options,
		});
	}

	/**
	 * @method init
	 */
	init() {
		super.init();

		this.$formula = this.$root.find('#formula');

		this.$on('table:select', $cell => {
			this.$formula.text($cell.data.value);
		});
	}

	/**
	 * @method toHTML
	 *
   * @return {string}
   */
	toHTML() {
		return `
		<div class="excel__formula-info">fx</div>
		<div 
			id="formula" 
			class="excel__formula-input" 
			contenteditable="true" spellcheck="false"
		>
		</div>`;
	}

	/**
	 * @method storeChanged
	 *
	 * @param {*} changes
	 */
	storeChanged({currentText}) {
		this.$formula.text(currentText);
	}

	/**
	 * @method onInput
	 *
	 * @param {*} event
	 */
	onInput(event) {
		this.$emit('formula:input', $(event.target).text());
	}

	/**
	 * @method onKeydown
	 *
	 * @param {Event} event
	 */
	onKeydown(event) {
		const keys = ['Enter', 'Tab'];
		const {key} = event;

		if (keys.includes(key)) {
			event.preventDefault();

			this.$emit('formula:done');
		}
	}
}
