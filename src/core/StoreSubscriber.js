import {isEqual} from '@core/utils';

/**
 * @class StoreSubscribe
 */
export class StoreSubscribe {
	/**
	 * @method constructor
   *
   * @param {store} store
   */
	constructor(store) {
		this.store = store;
		this.sub = null;
		this.previousState = {};
	}

	/**
	 * @method subscribeComponent
   *
   * @param {*} components
   */
	subscribeComponents(components) {
		this.previousState = this.store.getState();
		this.sub = this.store.subscribe( state => {
			Object.keys(state).forEach(key => {
				if (isEqual(this.previousState[key], state[key])) {
					components.forEach(component => {
						if (component.isWatchining(key)) {
							const changes = {[key]: state[key]};
							component.storeChanged(changes);
						}
					});
				}
			});

			this.previousState = this.store.getState();

			if (process.env.NODE_ENV === 'development') {
				window['redux'] = this.previousState;
			}
		});
	}

	/**
   * @method unsubscribeFromStore
   */
	unsubscribeFromStore() {
		this.sub.unsubscribe();
	}
}
