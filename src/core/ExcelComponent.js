import {DomListener} from '@core/DomListener';

/**
 * @class ExcelComponent
 */
export class ExcelComponent extends DomListener {
	/**
	 * @method constructor
	 *
	 * @param {*} $root
	 * @param {*} options
	 */
	constructor($root, options = {}) {
		super($root, options.listeners);
		this.name = options.name;
		this.store = options.store;
		this.emmiter = options.emmiter;
		this.subscribe = options.subscribe || '';
		this.unsubscribers = [];

		this.prepare();
	}

	/**
	 * @method prepare
	 */
	prepare() {}

	/**
	 * @method toHTML
	 *
   * @return {string}
	 */
	toHTML() {
		return '';
	}

	/**
	 * @method $emit
	 *
	 * @param {*} eventName
	 * @param  {...any} args
	 */
	$emit(eventName, ...args) {
		this.emmiter.emit(eventName, ...args);
	}

	/**
	 * @method $on
	 *
	 * @param {*} eventName
	 * @param {*} fn
	 */
	$on(eventName, fn) {
		const unsub = this.emmiter.subscribe(eventName, fn);
		this.unsubscribers.push(unsub);
	}

	/**
	 * @method $dispatch
	 *
	 * @param {*} action
	 */
	$dispatch(action) {
		this.store.dispatch(action);
	}

	/**
	 * @method storeChanged
	 *
	 * @param {*} changes
	 */
	storeChanged(changes) {

	}

	/**
	 * @method isWatchining
	 *
	 * @param {*} key
	 *
	 * @return {boolean}
	 */
	isWatchining(key) {
		return this.subscribe.includes(key);
	}

	/**
	 * @method init
	 */
	init() {
		this.initDOMListener();
	}

	/**
   * @method destroy
   */
	destroy() {
		this.removeDOMListener();
		this.unsubscribers.forEach(unsub => unsub());
	}
}
