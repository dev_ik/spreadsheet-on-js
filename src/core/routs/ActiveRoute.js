
/**
 * @class ActiveRoute
 */
export class ActiveRoute {
	/**
	 * @method path
	 *
	 * @return {string}
	 */
	static get path() {
		return window.location.hash.slice(1);
	}

	/**
	 * @method param
	 *
	 * @return {string[]}
	 */
	static get param() {
		return ActiveRoute.path.split('/')[1];
	}

	/**
	 * @method navigate
	 *
	 * @param {string} path
	 */
	static navigate(path) {
		window.location = path;
	}
}
