import {$} from '../dom';
import {ActiveRoute} from './ActiveRoute';
import {Loader} from '../../components/Loader';

/**
 * @class Router
 */
export class Router {
	/**
	 * @method constructor
	 *
	 * @param {*} selector
	 * @param {Object} routes
	 */
	constructor(selector, routes) {
		if (!selector) {
			throw new Error('Selector is not provider in Router');
		}

		this.$placeholder = $(selector);
		this.routes = routes;

		this.loader = new Loader();

		this.page = null;

		this.changePageHandler = this.changePageHandler.bind(this);

		this.init();
	}

	/**
	 * @method init
	 */
	init() {
		window.addEventListener('hashchange', this.changePageHandler);
		this.changePageHandler();
	}

	/**
	 * @method changePageHandler
	 */
	async changePageHandler() {
		if (this.page) {
			this.page.destroy();
		}

		this.$placeholder.clear().append(this.loader);

		const Page = ActiveRoute.path.includes('excel')
			? this.routes.excel
			: this.routes.dashboard;

		this.page = new Page(ActiveRoute.param);

		const root = await this.page.getRoot();

		this.$placeholder.clear().append(root);

		this.page.afterRender();
	}

	/**
	 * @method destroy
	 */
	destroy() {
		window.removeEventListener('hashchange', this.changePageHandler);
	}
}
