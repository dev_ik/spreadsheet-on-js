import {Router} from './Router';
import {Page} from '../page/Page';

/**
 * @class DashboardPageMock
 */
class DashboardPageMock extends Page {
	/**
	 * @method getRoot
	 *
	 * @return {HTMLDivElement}
	 */
	getRoot() {
		const root = document.createElement('div');
		root.innerHTML = 'dashboard';
		return root;
	}
}

/**
 * @class ExcelPageMock
 */
class ExcelPageMock extends Page {
	/**
	 * @method getRoot
	 *
	 * @return {HTMLDivElement}
	 */
	getRoot() {
		const root = document.createElement('div');
		root.innerHTML = 'excel';
		return root;
	}
}

describe('Router', () => {
	let router;
	let $root;

	/**
	 * @method beforeEach
	 */
	beforeEach(() => {
		$root = document.createElement('div');
		router = new Router( $root, {
			dashboard: DashboardPageMock,
			excel: ExcelPageMock,
		});
	});

	/**
	 * @method test
	 */
	test('should be defined', () => {
		expect(router).toBeDefined();
	});

	/**
	 * @method test
	 */
	test('should render dashboard page', () => {
		router.changePageHandler();
		expect($root.innerHTML).toBe('<div>dashboard</div>');
	});

	/**
	 * @method test
	 */
	test('should render excel page', () => {
		window.location.hash = '#excel/1';
		router.changePageHandler();
		expect($root.innerHTML).toBe('<div>excel</div>');
	});
});
