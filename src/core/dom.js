/**
 * @class Dom
 */
class Dom {
	/**
	 * @method constructor
	 *
	 * @param {*} selector
	 */
	constructor(selector) {
		this.$el = typeof selector === 'string'
			? document.querySelector(selector)
			: selector;
	}

	/**
	 * @method html
	 *
	 * @param {*} html
	 *
	 * @return {Dom|string}
	 */
	html(html) {
		if (typeof html === 'string') {
			this.$el.innerHTML = html;
			return this;
		}

		return this.$el.outerHTML.trim();
	}

	/**
	 * @method text
	 *
	 * @param {*} text
	 *
	 * @return {this|string}
	 */
	text(text) {
		if (typeof text !== 'undefined') {
			this.$el.textContent = text;
			return this;
		}
		if (this.$el.tagName.toLowerCase() === 'input') {
			return this.$el.value.trim();
		}
		return this.$el.textContent.trim();
	}

	/**
	 * @method clear
	 *
	 * @method clear
	 *
	 * @return {Dom}
	 */
	clear() {
		this.html('');
		return this;
	}

	/**
	 * @method on
	 *
	 * @param {*} eventType
	 * @param {*} callback
	 */
	on(eventType, callback) {
		this.$el.addEventListener(eventType, callback);
	}

	/**
	 * @method off
	 *
	 * @param {*} eventType
	 * @param {*} callback
	 */
	off(eventType, callback) {
		this.$el.removeEventListener(eventType, callback);
	}

	/**
	 * @method append
	 *
	 * @param {*} node
	 *
	 * @return {Dom}
	 */
	append(node) {
		if (node instanceof Dom) {
			node = node.$el;
		}
		if (Element.prototype.append) {
			this.$el.append(node);
		} else {
			this.$sel.appendChild(node);
		}

		return this;
	}

	/**
	 * @method closest
	 *
	 * @param {*} selector
	 * @return {Dom}
	 */
	closest(selector) {
		return $(this.$el.closest(selector));
	}

	/**
	 * @method getCoords
	 *
	 * @return {DOMRect}
	 */
	getCoords() {
		return this.$el.getBoundingClientRect();
	}

	/**
	 * @method findAll
	 *
	 * @param {*} selector
	 *
	 * @return {Object}
	 */
	findAll(selector) {
		return this.$el.querySelectorAll(selector);
	}

	/**
	 * @method find
	 *
	 * @param {*} selector
	 *
	 * @return {Dom}
	 */
	find(selector) {
		return $(this.$el.querySelector(selector));
	}

	/**
	 * @method css
	 *
	 * @param {Object} styles
	 */
	css(styles = {}) {
		Object.keys(styles)
			.forEach(key => this.$el.style[key] = styles[key]);
	}

	/**
	 * @method addClass
	 *
	 * @param {string} className
	 */
	addClass(className) {
		this.$el.classList.add(className);
	}

	/**
	 * @method removeClass
	 *
	 * @param {string} className
	 */
	removeClass(className) {
		this.$el.classList.remove(className);
	}

	/**
	 * @method data
	 *
	 * @return {Object}
	 */
	get data() {
		return this.$el.dataset;
	}

	/**
	 * @method getStyles
	 *
	 * @param {Array} styles
	 * @return {*}
	 */
	getStyles(styles = []) {
		return styles.reduce((res, currentStyle) => {
			res[currentStyle] = this.$el.style[currentStyle];
			return res;
		}, {});
	}

	/**
	 * @method id
	 *
	 * @param {*} parse
	 *
	 * @return {string}
	 */
	id(parse) {
		if (parse) {
			const parsed = this.id().split(':');
			return {
				row: +parsed[0],
				col: +parsed[1],
			};
		}
		return this.data.id;
	}

	/**
	 * @method attr
	 *
	 * @param {string} name
	 * @param {*} value
	 * @return {string|Dom}
	 */
	attr(name, value) {
		if (value) {
			this.$el.setAttribute(name, value);
			return this;
		}
		return this.$el.getAttribute(name);
	}

	/**
	 * @method focus
	 *
	 * @return {Dom}
	 */
	focus() {
		this.$el.focus();
		return this;
	}
}

/**
 *
 * @param {*} selector
 * @return {Dom}
 */
export function $(selector) {
	return new Dom(selector);
}

$.create = (tagName, classes = '') => {
	const el = document.createElement(tagName);
	if (classes) {
		el.classList.add(classes);
	}

	return $(el);
};
