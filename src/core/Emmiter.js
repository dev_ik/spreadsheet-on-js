/**
 *@class Emmiter
 */
export class Emmiter {
	/**
   * @method constructor
   */
	constructor() {
		this.listeners = {};
	}

	/**
	 * @method emit
	 *
   * @param {string} eventName
   *
   * @return {boolean}
   */
	emit(eventName, ...args) {
		if (!Array.isArray(this.listeners[eventName])) {
			return false;
		}
		this.listeners[eventName].forEach(listener => {
			listener(...args);
		});

		return true;
	}

	/**
	 * @method subscribe
	 *
   * @param {string} eventName
   * @param {Function} fn
   *
   * @return {Function}
   */
	subscribe(eventName, fn) {
		this.listeners[eventName] = this.listeners[eventName] || [];
		this.listeners[eventName].push(fn);

		return () => {
			this.listeners[eventName] =
            this.listeners[eventName].filter(listener => listener !== fn);
		};
	}
}
