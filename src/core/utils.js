/**
 *
 * @param {*} string
 * @return {string}
 */
export function capitalize(string) {
	if (typeof string !== 'string') {
		return '';
	}
	return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 *
 * @param {number} start
 * @param {number} end
 *
 * @return {Array}
 */
export function range(start, end) {
	if (start > end) {
		[end, start] = [start, end];
	}

	return new Array(end - start +1)
		.fill('')
		.map((_, index) => start + index);
}

/**
 *
 * @param {*} $target
 * @param {*} $current
 *
 * @return {Array}
 */
export function matrix($target, $current) {
	const target = $target.id(true);
	const current = $current.id(true);

	const cols = range(current.col, target.col);
	const rows = range(current.row, target.row);

	return cols.reduce((acc, col) => {
		rows.forEach(row => acc.push(`${row}:${col}`));
		return acc;
	}, []);
}

/**
 *
 * @param {string} key
 * @param {string} id
 *
 * @return {selector}
 */
export function nextSelector(key, {col, row}) {
	const MIN_VALUE = 0;
	switch (key) {
	case 'Enter':
	case 'ArrowDown':
		row++;
		break;
	case 'Tab':
	case 'ArrowRight':
		col++;
		break;
	case 'ArrowLeft':
		col = col - 1 < MIN_VALUE ? MIN_VALUE : col - 1;
		break;
	case 'ArrowUp':
		row = row - 1 < MIN_VALUE ? MIN_VALUE : row - 1;
		break;
	}
	return `[data-id="${row}:${col}"]`;
}

/**
 *
 * @param {string} key
 * @param {*} data
 *
 * @return {*}
 */
export function storage(key, data = null) {
	if (!data) {
		return JSON.parse(localStorage.getItem(key));
	}
	localStorage.setItem(key, JSON.stringify(data));
}

/**
 *
 * @param {*} previous
 * @param {*} current
 *
 * @return {bool}
 */
export function isEqual(previous, current) {
	if (typeof previous === 'object' && typeof current === 'object') {
		return JSON.stringify(previous) !== JSON.stringify(current);
	}
	return previous !== current;
}

/**
 *
 * @param {string} str
 * @return {string}
 */
export function camelToDashCase(str) {
	return str.replace(/([A-Z])/g, g => `-${g[0].toLowerCase()}`);
}

/**
 *
 * @param {Object} styles
 * @return {string}
 */
export function toInlineStyles(styles = {}) {
	return Object.keys(styles)
		.map(key => `${camelToDashCase(key)}: ${styles[key]}`)
		.join(';');
}

/**
 *
 * @param {Function} fn
 * @param {number} wait
 * @return {function(...[*]=)}
 */
export function debounce(fn, wait) {
	let timeout;
	return function(...args) {
		const later = () => {
			clearTimeout(timeout);
			// eslint-disable-next-line no-invalid-this
			fn.apply(this, args);
		};
		clearTimeout(timeout);
		timeout = setTimeout( later, wait);
	};
}

/**
 *
 * @param {Object} obj
 * @return {Object}
 */
export function clone(obj) {
	return JSON.parse(JSON.stringify(obj));
}

/**
 *
 * @param {Event} event
 */
export function preventDefault(event) {
	event.preventDefault();
}
