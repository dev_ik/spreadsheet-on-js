import {capitalize} from './utils';

/**
 * @class DomListener
 */
export class DomListener {
	/**
	 * @method constructor
   *
   * @param {*} $root,
	 * @param {Array} listeners
	 *
   */
	constructor($root, listeners = []) {
		if (!$root) {
			throw new Error('No root provider for DomListener');
		}
		this.$root = $root;
		this.listeners = listeners;
	}

	/**
	 * @method initDOMListener
	 */
	initDOMListener() {
		this.listeners.forEach(listener => {
			const method = getMethodName(listener);

			if (!this[method]) {
				throw new Error(`
				Method ${method} is not implemented ${this.name} Component`);
			}

			this[method] = this[method].bind(this);

			this.$root.on(listener, this[method]);
		});
	}

	/**
	 * @method removeDOMListener
	 */
	removeDOMListener() {
		this.listeners.forEach(listener => {
			const method = getMethodName(listener);
			this.$root.off(listener, this[method]);
		});
	}
}

/**
 *
 * @param {*} eventName
 * @return {string}
 */
function getMethodName(eventName) {
	return 'on' + capitalize(eventName);
}
