import {debounce} from '@core/utils';

/**
 * @class StateProcessor
 */
export class StateProcessor {
	/**
	 * @method constructor
	 *
	 * @param {Object} client
	 * @param {number} delay
	 */
	constructor(client, delay = 300) {
		this.client = client;
		this.listen = debounce(this.listen.bind(this), delay);
	}

	/**
	 * @method listen
	 *
	 * @param {Object} state
	 */
	listen(state) {
		this.client.save(state);
	}

	/**
	 * @method get
	 *
	 * @return {*}
	 */
	get() {
		return this.client.get();
	}
}
