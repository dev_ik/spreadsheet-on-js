/**
 * @class Page
 */
export class Page {
	/**
	 * @method constructor
	 *
	 * @param {*} params
	 */
	constructor(params) {
		this.params = params || Date.now().toString();
	}

	/**
	 * @method getRoot
	 *
	 * @throws Error
	 */
	getRoot() {
		throw new Error('Method "getRoot" should be implemented');
	}

	/**
	 * @method afterRender
	 */
	afterRender() {}

	/**
	 * @method destroy
	 */
	destroy() {}
}
