import {createStore} from './createStore';

/**
 *
 * @type {{count: number}}
 */
const initialState = {
	count: 0,
};

/**
 *
 * @param {Object} state
 * @param {string} action
 * @return {{count: number}}
 */
const reducer = (state = initialState, action) => {
	if (action.type === 'ADD') {
		return {...state, count: ++state.count};
	}

	return state;
};

describe('createStore', () => {
	let store;
	let handler;

	/**
	 *@method beforeEach
	 */
	beforeEach(() => {
		store = createStore(reducer, initialState);
		handler = jest.fn();
	});

	/**
	 * @method test
	 */
	test('should return store object', () => {
		expect(store).toBeDefined();
		expect(store.dispatch).toBeDefined();
		expect(store.subscribe).toBeDefined();
		expect(store.getState).not.toBeUndefined();
	});

	/**
	 * @method test
	 */
	test('should return object as a state', () => {
		expect(store.getState()).toBeInstanceOf(Object);
	});

	/**
	 * @method test
	 */
	test('should return default state', () => {
		expect(store.getState()).toEqual(initialState);
	});

	/**
	 * @method test
	 */
	test('should change state if action exists', () => {
		store.dispatch({type: 'ADD'});
		expect(store.getState().count).toBe(1);
	});

	/**
	 * @method test
	 */
	test('should not change state if action not exists', () => {
		store.dispatch({type: 'NOT_EXISTING_ACTION'});
		expect(store.getState().count).toBe(0);
	});

	/**
	 * @method test
	 */
	test('should call subscriber function', () => {
		store.subscribe(handler);
		store.dispatch({type: 'ADD'});

		expect(handler).toHaveBeenCalled();
		expect(handler).toHaveBeenCalledWith(store.getState());
	});

	/**
	 * @method test
	 */
	test('should not call sub if unsubscribe', () => {
		const sub = store.subscribe(handler);

		sub.unsubscribe();

		store.dispatch({type: 'ADD'});

		expect(handler).not.toHaveBeenCalled();
		expect(handler).not.toHaveBeenCalledWith(store.getState());
	});

	/**
	 * @method test
	 */
	test('should dispatch in async way', () => {
		return new Promise(resolve => {
			setTimeout(() => {
				store.dispatch({type: 'ADD'});
			}, 500);

			setTimeout(() => {
				expect(store.getState().count).toBe(1);
				resolve();
			}, 1000);
		});
	});
});
