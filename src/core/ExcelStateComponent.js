import {ExcelComponent} from '@core/ExcelComponent';

/**
 * @class ExcelStateComponent
 */
export class ExcelStateComponent extends ExcelComponent {
	/**
	 * @method constructor
	 *
	 * @param {*} args
	 */
	constructor(...args) {
		super(...args);
	}

	/**
	 * @method initState
	 *
	 * @param {Object} initialState
	 */
	initState(initialState = {}) {
		this.state = {...initialState};
	}

	/**
	 * @method template
	 *
	 * @return {string}
	 */
	get template() {
		return JSON.stringify(this.state, null, 2);
	}

	/**
	 * @method setState
	 *
	 * @param {Object} newState
	 */
	setState(newState) {
		this.state = {...this.state, ...newState};
		this.$root.html(this.template);
	}
}
