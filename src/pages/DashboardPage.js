import {Page} from '@core/page/Page';
import {$} from '@core/dom';
import {getAllRecords} from '@/shared/dashboard.function';

/**
 * @class DashboardPage
 */
export class DashboardPage extends Page {
	/**
	 * @method getRoot
	 *
	 * @return {string}
	 */
	getRoot() {
		const now = Date.now().toString();
		return $.create('div', 'db')
			.html(`<div class="db__header">
				  <h1>Excel</h1>
				</div>
				
				<div class="db__new">
				  <div class="db__view">
				    <a href="#excel/${now}" class="db__create">
				      Новая <br/>
				      таблица
				    </a>
				  </div>
				</div>
		
				<div class="db__table db__view">
				  ${getAllRecords()}
				</div>`);
	}
}
