import {Page} from '@core/page/Page';
import {StateProcessor} from '@core/page/StateProcessor';
import {Excel} from '@/components/excel/Excel';
import {Header} from '@/components/header/Header';
import {Toolbar} from '@/components/toolbar/Toolbar';
import {Formula} from '@/components/formula/Formula';
import {Table} from '@/components/table/Table';
import {createStore} from '@core/store/createStore';
import {rootReducer} from '@/redux/rootReducer';
import {normalizeInitialState} from '@/redux/initialState';
import {LocalStorageClient} from '@/shared/clients/LocalStorageClient';

/**
 * @class ExcelPage
 */
export class ExcelPage extends Page {
	/**
	 * @method constructor
	 *
	 * @param {*} param
	 */
	constructor(param) {
		super(param);

		this.storeSub = null;
		this.processor = new StateProcessor(
			new LocalStorageClient(this.params)
		);
	}

	/**
	 * @method getRoot
	 *
	 * @return {string}
	 */
	async getRoot() {
		const state = await this.processor.get();
		const store = createStore(rootReducer, normalizeInitialState(state));

		this.storeSub = store.subscribe(this.processor.listen);

		this.excel = new Excel({
			components: [Header, Toolbar, Formula, Table],
			store,
		});

		return this.excel.getRoot();
	}

	/**
	 * @method afterRender
	 */
	afterRender() {
		this.excel.init();
	}

	/**
	 * @method destroy
	 */
	destroy() {
		this.excel.destroy();
		this.storeSub.unsubscribe();
	}
}
